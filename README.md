# Drupal 7

A website created by [adHOME Creative](https://adhomecreative.com) built responsively with AODA Level AA compliance using Drupal 7 and customized Kwicks JS sliding panels.

Kwicks Sliding Panels was custom programming [to allow for horizontal sliding on inner pages](http://www.healthyuoft.ca/student-outreach) - a feature of the library which was not available at the time.


[HealthyU](http://www.healthyuoft.ca)

[AODO Website](https://accessontario.com/aoda)

[Kwicks JS Sliding Panels](https://github.com/jmar777/kwicks)

